import Vue from "vue";
import Router from "vue-router";
import Home from "./components/Home.vue";
import Game from "./components/Game.vue";
import Results from "./components/Results.vue";

Vue.use(Router); // Add the Router features to the Vue Object

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/game",
    component: Game,
  },
  {
    path: "/results",
    component: Results,
  },
];

export default new Router({ routes });
